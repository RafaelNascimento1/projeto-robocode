# Robocode

Olá, Boa tarde,

 Estou muito feliz em finalmente ter terminado o meu robô, eu estou muito ansioso para receber o feedback de vocês.

## Sobre o Robô

 O código implementado no meu robô foi pensado em um robô para focar unicamente em um inimigo, e que girasse em torno dele para que não fosse alvejado facilmente. Os pontos fortes do meu robô é a alta mobilidade e tambem a possibilidade de manter o robô inimigo na mira.
 
## Pontos fortes e Fracos
 
  o principal ponto fraco dele é, se caso o robo inimigo esteja longe e se movimente ele acaba errando o tiro, ele é facilmente alvejado por outro robo pois ele foca unicamente em um inimigo até ele ser destruído.

## Principais dificuldades

 Minha principal dificuldade foi unir 3 partes diferente de robôs (mira, mobilidade e foco), e após ter pesquisado bastante eu consegui unir os três, porêm o compilador que havia no robocode não estava compilando por causa do comando "import static" pois a versão do compilador precisava ser 1.5 ou mais recente, encontrei em um forum dizendo que precisava do java para desenvolvedores, após ter baixado tive que adicionar a pasta /bin nas variaveis de ambiente do windows, ai sim eu consegui compilar.

## Sobre a experiência

 O que eu mais gostei foi de ter sido desafiado em uma coisa q eu nem sabia oq significava um comando if ou public void run, tive que pesquisar bastante para ter uma noção basica dos comandos java e tambem do robocode, eu me diverti muito mas ao mesmo tempo que eu me divertia me estressava bastante quando dava um erro na compilação do meu robô. Mas espero que gostem! Ah, me desculpe se caso meu código está bem vazio em relação a comentar cada processo feito nele, pois o meu primeiro robo está no computador e eu estou me mudando para outra cidade, mas se caso vocês queiram dar uma olhada depois so mandar um email. 
