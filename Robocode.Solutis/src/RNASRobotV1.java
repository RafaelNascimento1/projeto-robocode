package rnasRobotV1;
import robocode.*;
import java.awt.Color;
import robocode.util.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;
/**
 * SoulKiing - Um Robô Desenvolvido por Rafael Nascimento.
 */
public class RNASRobotV1 extends AdvancedRobot
{
	boolean andandoParaFrente;
	boolean inWall;


	public void run() {																	//Após inicio do round os comandos abaixo irão ser lidos.
	
		setBodyColor(Color.RED);
		setBulletColor(Color.WHITE);
		setGunColor(Color.WHITE);														//Nesse caso estou mudando as cores do Robô.
		setRadarColor(Color.GRAY);
		setScanColor(Color.YELLOW);
		setAdjustRadarForRobotTurn(true);
		setAdjustGunForRobotTurn(true); 												 //isso serve para o Radar e a arma girarem de forma independente.
		setAdjustRadarForGunTurn(true);
		
			if(getX() <= 30 || getY() <= 30												 //|| OU (“logical OR”) a || b -> retorna true se a ou b for true. Senão retorna false. Se a for true, b não é avaliada.
							|| getBattleFieldWidth() - getX() <= 30
							|| getBattleFieldHeight() - getY() <=30) {
			
				this.inWall = true;														 //"this.variable" refere-se ao objeto atual.
			}
		
				else {
				this.inWall = false;
			}
			

		setAhead (10000); 																//estou solicitando que o robô andar 10000 pixels para frente.
		setTurnRadarRight(360); 														// estou solicitando que o Radar do robô gire o radar 360 graus para localizar o inimigo.
		this.andandoParaFrente = true;													
	

		while(true) {
		
			if (getX() > 30 && getY() > 30
							&& getBattleFieldWidth() - getX() > 30						//Se a distância do robo em relação à parede for maior que 30 pixels o robo ira executar o comando dentro dos {}.
							&& getBattleFieldHeight() - getY() > 30						//&& E (“logical AND”) a && b -> retorna true se a e b forem ambos true. Senão retorna false. Se a for false, b não é avaliada.
							&& this.inWall == true) {									//enquanto o robo esteja longe da parede a flag estará setada para True, caso contrario ira mudar para false e rodará o codigo abaixo.
				
				this.inWall = false;
			
			}
			
			if (getX() <= 30 || getY() <= 30											// caso caia nessa condição e porque o robo esta a menos de 30px em relação a parede X e/ou Y.
							 || getBattleFieldWidth() - getX() <= 30					// dentro dessa variavel "if" estará os comandos para evitar que o robo bata na parede e assim escape de uma morte certa.
							 || getBattleFieldHeight() - getY() <= 30){					// nesse caso ele ira inverter a direção e ira mudar a variavel inWall para true novamente e quando estiver em true irá rodar a outra varivel acima.
							 
				if(this.inWall == false){
				
					reverseDirection();
					inWall = true;
				
				}
			}
			

			if(getRadarTurnRemaining() == 0.0) {										// se o radar do robo estiver ocioso irá ativar essa variavel para que se mova 360 graus.
			
				setTurnRadarRight(360);													// com o codigo "set" no inicio so será ativado caso tenha o codigo "execute".
				
			}
			execute();																	// o comando execute serve para que possa funcionar em conjunto com outros comandos.

		}
	}
	
		

	
	public void onScannedRobot(ScannedRobotEvent e) {
	
		double anguloAbsoluto = getHeading() + e.getBearing();										//esse calculo serve para saber a quantidade de graus que um inimigo está em relação a "face" do meu robô.
		double anguloDaArma = normalRelativeAngleDegrees(anguloAbsoluto - getGunHeading());			//aqui será subtraído o "anguloAbsoluto" com a direção (em graus) que esta a face da minha arma.
		double anguloDoRadar = normalRelativeAngleDegrees(anguloAbsoluto - getRadarHeading());		//esse caso e bem similiar ao anterior so muda que irá usar o angulo do radar para subtrair.
		
		if (this.andandoParaFrente) { //se o meu robô estiver andando para frente ele irá executar essa condição.
			setTurnRight (normalRelativeAngleDegrees(e.getBearing() + 80));
		}
		else { //caso não esteja, irá excecutar essa condição.
			setTurnRight(normalRelativeAngleDegrees(e.getBearing() + 100));
		}

		if(Math.abs(anguloDaArma) <= 4) { //nesse caso estou solicitando um valor absoluto para se caso eu estiver proximo ao inimigo possa disparar com precisão.
			setTurnGunRight(anguloDaArma);
			setTurnRadarRight(anguloDoRadar);
		}	
			if(getGunHeat() == 0 && getEnergy() > .2) { //se a temperatura do canhão estiver zerado e a energia do tank for maior que .2 o canhão irá disparar.
				fire(Math.min(
						4.5 - Math.abs(anguloDaArma) / 2 - e.getDistance() / 250, getEnergy() - .1));		//esse calculo e utilizando para que não haja muita perda de energia ao disparar.

			}
		
		else{ //caso não seja possivel disparar a arma com precisão será solicitado somente a aquisição do alvo.
			setTurnGunRight(anguloDaArma);
			setTurnRadarRight(anguloDoRadar);
		}		

		if(anguloDaArma ==0) {// se o angulo da arma for igual igual a zero, o robô irá ser obrigado a executar o "onScannedRobot".
			scan();
		}

	}

	public void onHitWall(HitWallEvent e) {
		reverseDirection();
	}
	
	public void onHitRobot (HitRobotEvent e) {											//Caso o robo bata em algum outro robo ele irá executar os comandos dentro dos {}.
		if(e.isMyFault()) {																// Nesse caso e uma variavel booleana que se caso meu robo entre em contato com outro robô automaticamente ele irá ativar a variavel e ira reverter a direção.
			reverseDirection();
		}	
	}
	
	public void reverseDirection() {
		if(this.andandoParaFrente) {
			setBack(40000);
			this.andandoParaFrente = false;
		}
		else {
			setAhead(40000);
			this.andandoParaFrente = true;
		}
	}	
}
